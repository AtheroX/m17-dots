﻿/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Burst;

public class MoveSystem : SystemBase {

	EntityCommandBufferSystem barrier => World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();

	protected override void OnUpdate() {

		var commandBuffer = barrier.CreateCommandBuffer().ToConcurrent();

		float dt = Time.DeltaTime;
		Entities.WithAny<BulletTag>().ForEach((Entity entity, int entityInQueryIndex, ref Translation trans, in Rotation rot, in MoveComponent moveComp) => {
			trans.Value += moveComp.speed * dt * math.mul(rot.Value, new float3(1, 0, 0));// math.forward(rot.Value);
		}).ScheduleParallel();

	}
}
