﻿/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using Unity.Entities;

[GenerateAuthoringComponent]
public struct PlayerTag : IComponentData { }
